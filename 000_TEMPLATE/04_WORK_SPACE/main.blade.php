@extends('<%=DNAME%>.base')

@section('title')
    Test
@endsection

@section('laravelBlock')
    @include('<%=DNAME%>.aa_include.zd_laravel_files')
@endsection

@section('cssBlock')

@endsection

@section('content')


    <div class="  container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('<%=DNAME%>.aa_include.zc_navbar')
            </div>

        </div>

    </div>




    <br>
    <br>
    @include('zz_body.<%=DNAME%>.body')




@endsection


@section('bottomJS')



@endsection


 
  