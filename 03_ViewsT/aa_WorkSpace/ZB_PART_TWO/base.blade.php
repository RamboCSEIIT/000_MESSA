<!DOCTYPE html>
<html lang=en>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv=ScreenOrientation content=autoRotate:disabled>
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name=description content="">
    <meta name=author content="">
    <link rel="shortcut icon" type=image/png href=02_IMAGES/favicon.png>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="01_CSS/icon-font.css">
    <style>  @include('01_CSS.zb_part_two') </style>
</head>


<body>
@yield('content')
<script type="text/javascript"> @include('00_SCRIPTS.zb_part_two')</script>
</body>

</html>