<header class="header">

    <div class="header__logo-box">
        <img src="03_IMAGES/ZA_PART_ONE/logo-white.png" alt="logo" class="header__logo">
    </div>

    <div class="header__text-box">
        <h1 class="heading-primary">
            <span class="heading-primary--main">{{  env('COMPANY_NAME')  }}</span>
            <span class="heading-primary--sub">{{  env('COMPANY_MOTO')  }}</span>
        </h1>
        <a href="#section-tours" class="btn btn--white btn--animated">Discover our tours</a>
    </div>

</header>