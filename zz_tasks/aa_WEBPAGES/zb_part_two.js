var SCRIPTS_OUT_PATH                                                = 'aa_template/resources/views/00_SCRIPTS';
var SCRIPTS_IN_PATH                                    = '02_SCRIPTS/ZB_PART_TWO/**/*.*';
var SCRIPTS_OUT_FILE_NAME                                               = 'zb_part_two.blade.php';


var SASS_OUT_PATH                                                       = 'aa_template/resources/views/01_CSS';
var SASS_IN_PATH                                      = '01_SASS/ZB_PART_TWO/style.scss';
var SASS_OUT_FILE_NAME                                                 = 'zb_part_two.blade.php';

var VIEW_T_IN_FRAG                         = '03_ViewsT/ab_Fragments/ZB_PART_TWO/**/*.*';
var VIEW_T_OUT_BODY                      = '03_ViewsT/aa_WorkSpace/zz_body/ZB_PART_TWO';
 

module.exports = 
{
  
  zb_part_two_mode : function ()
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  },
 get_function: function ()
 {
     return this.zb_part_two_mode ;
 }




};