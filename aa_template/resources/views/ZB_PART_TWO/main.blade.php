@extends('ZB_PART_TWO.base')

@section('title')
    TWO
@endsection

@section('laravelBlock')
   {{--   @include('ZA_PART_ONE.aa_include.zd_laravel_files')--}}
@endsection

@section('cssBlock')

@endsection

@section('content')
@include('ZB_PART_TWO.aa_include.zc_navbar')
<main>
@include('zz_body.ZB_PART_TWO.body')
</main>
@include('ZB_PART_TWO.aa_include.ze_footer')
@endsection


@section('bottomJS')



@endsection


 
  