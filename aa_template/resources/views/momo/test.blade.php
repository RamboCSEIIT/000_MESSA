<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type=image/png href="http://wayanadtoursandtravels.com/02_IMAGES/favicon.png">
    <link rel=stylesheet href=https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css type=text/css>
    <link rel=stylesheet href=https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css type=text/css>@yield('cssBlock')
    <title>@yield('title')</title>



   </head>
<body>



        @yield('content')

        <script src=https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js></script>
        <script src=https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js></script>
        <script src=https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js></script>


</body>
</html>
