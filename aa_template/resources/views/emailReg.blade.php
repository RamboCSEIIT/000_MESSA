@component('mail::message')


To verify your registration, please click
<a href="{{ route( 'sendEmailDone',[ "email" => $user->email,"verifyToken" => $user->VerifyToken ]) }}"> git here</a>




Thanks,<br>
{{ env('COMPANY_NAME') }}
@endcomponent
