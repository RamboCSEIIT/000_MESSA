/* * * * * * * ZA Part one * * * * */
/*   color*/
/*Light green */
/* MediumGreen */
/*$DarkGreen */
/*white */
/*grey */
/*grey */
/*black */
/*
0 - 600px:      Phone
600 - 900px:    Tablet portrait
900 - 1200px:   Tablet landscape
[1200 - 1800] is where our normal styles apply
1800px + :      Big desktop

$breakpoint arguement choices:
- phone
- tab-port
- tab-land
- big-desktop

ORDER: Base + typography > general layout + grid > page layout > components

1em = 16px
*/
#navabar-top .pale-pink
{
  background-color: #ffb6c1;
}

#navabar-top a
{
  color: white;
  text-decoration: none;
  margin-left: 40px;
  font-family: 'Roboto';
  border-bottom: 1px solid rgba(254, 254, 254, 0);
  -webkit-transition: border-bottom .5s;
  -o-transition: border-bottom .5s;
  transition: border-bottom .5s;
  -webkit-transition-timing-function: ease-in-out;
  -o-transition-timing-function: ease-in-out;
  transition-timing-function: ease-in-out;
}

#navabar-top a:hover
{
  border-bottom: 1px solid #fefefe;
  color: blue;
}

#sidebar-top a
{
  font-family: 'Roboto';
}

#sidebar-top .pale-pink
{
  background-color: #ffb6c1;
}

#sidebar-top .sidebar
{
  left: -310px;
  width: 250px;
  -webkit-transition: .6s;
  -o-transition: .6s;
  transition: .6s;
}

#sidebar-top .active
{
  left: 0;
}

#sidebar-top ul
{
  margin: 0;
  padding: 0px 0;
  position: relative;
}

#sidebar-top li
{
  list-style: none;
  /* text-align: center; */
  -webkit-transition: color 0.9s ease;
  -o-transition: color 0.9s ease;
  transition: color 0.9s ease;
  position: relative;
}

#sidebar-top a::before
{
  position: absolute;
  content: '';
  background: rgba(255, 0, 0, 0.5);
  display: block;
  position: absolute;
  width: 0%;
  height: 100%;
  -webkit-transition: .5s;
  -o-transition: .5s;
  transition: .5s;
}

#sidebar-top a:hover:before
{
  width: 100%;
  padding: 10px 0px;
}

#sidebar-top a
{
  color: #fff;
  display: block;
  text-decoration: none;
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  height: 60px;
  position: relative;
}

#sidebar-top span
{
  position: absolute;
  top: 50%;
  left: 50%;
  /*transform: translateXY(-50%);*/
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}

#sidebar-top .SideBarButton
{
  position: absolute;
  top: 0;
  right: -50px;
  width: 50px;
  height: 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  cursor: pointer;
  border: none;
  outline: none;
}

#sidebar-top .SideBarButton span
{
  display: block;
  width: 35px;
  height: 3px;
  position: absolute;
  top: 24px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-top .SideBarButton span:before
{
  content: '';
  position: absolute;
  top: -10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-top .SideBarButton span:after
{
  content: '';
  position: absolute;
  top: 10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-top .SideBarButton.toggle span
{
  background: transparent;
}

#sidebar-top .SideBarButton.toggle span:before
{
  top: 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

#sidebar-top .SideBarButton.toggle span:after
{
  top: 0;
  -webkit-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

#sidebar-nav-menu .pale-pink
{
  background-color: #ffb6c1;
}

#sidebar-nav-menu .pale-pink_trans
{
  background-color: rgba(255, 182, 193, 0.7);
}

#sidebar-nav-menu .SideBarButton
{
  position: absolute;
  width: 50px;
  height: 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  cursor: pointer;
  border: none;
  outline: none;
}

#sidebar-nav-menu .SideBarButton span
{
  display: block;
  width: 35px;
  height: 3px;
  position: absolute;
  top: 24px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-nav-menu .SideBarButton span:before
{
  content: '';
  position: absolute;
  top: -10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-nav-menu .SideBarButton span:after
{
  content: '';
  position: absolute;
  top: 10px;
  left: 0;
  width: 100%;
  height: 3px;
  background: white;
  -webkit-transition: .3s;
  -o-transition: .3s;
  transition: .3s;
}

#sidebar-nav-menu .SideBarButton.toggle span
{
  background: transparent;
}

#sidebar-nav-menu .SideBarButton.toggle span:before
{
  top: 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

#sidebar-nav-menu .SideBarButton.toggle span:after
{
  top: 0;
  -webkit-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

#sidebar-nav-menu .navbar-brand
{
  position: absolute;
  left: 50%;
  -webkit-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
}

#sidebar-nav-menu .hamberger-menu
{
  position: absolute;
  -webkit-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
  margin-left: 3%;
}

#sidebar-nav-menu .logo-background
{
  background-color: #ffb6c1;
}

#sidebar-nav-menu .circle-bg
{
  margin: 20px;
}
