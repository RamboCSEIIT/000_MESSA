<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_admin extends Model
{
    //

    protected $fillable = [
        'role_id', 'admin-id'
    ];
}
