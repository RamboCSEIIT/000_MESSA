<?php

namespace App\Http\Controllers\AA_ADMIN_AA_HOME;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class index extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        //miss manage wrong links
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {


        return view('AA_ADMIN_AA_HOME.dashboard');

        // return view('AA_ADMIN_AA_HOME.main');
    }

}
