<?php

namespace App\Http\Controllers\Auth;

use App\Mail\registerEmail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

use Mail;
use App\Mail\verifyEmail;

use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        /*
        return User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'VerifyToken'=> Str::random(40)
        ]);*/

        Session::flash('status',"Registered !! Open your mail and verify account");

        $user = User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'VerifyToken'=> Str::random(40)
        ]);



        $thisUser= User::findOrFail($user->id);

       // dd($thisUser);

        $this->sendEmail($thisUser);

        return $user;



    }


    public function verifyEmailFirst( )
    {
        return  view('email.verifyEmailFirst');
    }

    public function sendEmail($thisUser)
    {

        // dd($thisUser);

      // dd( $thisUser['email']);

     //   dd($thisUser->user['email']);

      Mail::to($thisUser['email'])->send((new registerEmail($thisUser))->from(env('COMPANY_EMAIL'))->subject("Registration Verification Mail ". env('COMPANY_NAME')));
    }

    public function sendEmailDone($email,$VerifyToken  )
    {
          // return $verifyToken;
          $user = User::where([ "email" => $email,"verifyToken" => $VerifyToken ])->first();
          if($user)
          {
                  Session::flash('status',"Verified Account!! Please login");
                    User::where([ "email" => $email,"verifyToken" => $VerifyToken ])->update([ "status" => "1","verifyToken" => NULL ]);

              return redirect(route('login'));
          }
          else
          {
                   return "Verification link  is not valid or expired";
          }

    }
}
